#!/usr/bin/python3

# SPDX-License-Identifier: GPL-2.0-or-later
# top-like utility for displaying kvm statistics

from utils import *

sidebar_chars = 25
cpu_count_chars = 5

from utils import *
from device import Device
from terminal import T
import curses

class IRQChip:
    def __init__(self, name, cpuinfo, totalmode, sum_cores, separators, max_columns):
        self.name = name
        self.cpuinfo = cpuinfo
        self.totalmode = totalmode
        self.sum_cores = sum_cores
        self.separators = separators
        self.max_columns = max_columns
        # all devices which belong to this irqchip
        self.devices = {}

    def set_name(self, name):
        self.name = name

    ################################################################################################

    def get_device(self, device_name):
        if not device_name in self.devices:
            self.devices[device_name] = Device(device_name)
        return self.devices[device_name]

    ################################################################################################

    def __print_conditional_newline(self, col, color = 0):

        if self.max_columns == -1:
            return

        if col > 0 and (col) % self.max_columns == 0:
            T.print("")
            T.print("".ljust(sidebar_chars + 1), newline = 0)
            T.print("".ljust(cpu_count_chars), newline = 0, option = color)
            T.print(" ", newline = 0)


    def __print_conditional_space(self, col):

        if self.max_columns != -1:
            col = col % self.max_columns

        if col in self.separators:
            T.print(" ", newline = 0)

    ################################################################################################

    def __print_app_header(self):
        T.print("")

        for thread in range(0, self.cpuinfo.max_thread_count):

            T.print("".ljust(sidebar_chars + 1), newline = 0)

            logical_core_ids = self.cpuinfo.get_logical_cores_for_thread(thread)

            if thread == 0:
                T.print("Σ".ljust(cpu_count_chars), newline = 0)
            else:
                T.print(" ".ljust(cpu_count_chars), newline = 0)

            T.print(" ", newline = 0)

            for i, logical_core in enumerate(logical_core_ids):
                self.__print_conditional_newline(i)
                self.__print_conditional_space(i)

                if logical_core == -1:
                    logical_core_str = "x"
                else:
                    logical_core_str = str(logical_core)

                T.print(logical_core_str.ljust(cpu_count_chars + 1), newline = 0)

            T.print("")

    ################################################################################################

    def __print_header(self):
        T.print(self.name.ljust(sidebar_chars), option = curses.A_REVERSE, newline = 0)

        T.print(" ", newline = 0)
        T.print("".ljust(cpu_count_chars), option = curses.A_REVERSE, newline = 0)
        T.print(" ", newline = 0)

        logical_core_ids = self.cpuinfo.get_logical_cores_for_thread(0)

        for i, _ in enumerate(logical_core_ids):
            self.__print_conditional_space(i)
            T.print("".ljust(cpu_count_chars + 1), newline = 0, option = curses.A_REVERSE)
            if i == self.max_columns - 1:
                break

        T.print("")

    ################################################################################################

    def __print_device(self, device, color):

        values = device.read(self.totalmode)
        rows_to_print = []

        if values == None:
            ncores = len(self.cpuinfo.get_logical_cores_for_thread(0))
            dummy_row = [ None ] * ncores
            rows_to_print = [ dummy_row ]

        else:
            for thread in range(0, self.cpuinfo.max_thread_count):
                logical_core_ids = self.cpuinfo.get_logical_cores_for_thread(thread)
                row = [ self.cpuinfo.get_proc_irq_value(values, logical_core_id)  for logical_core_id in logical_core_ids ]
                rows_to_print.append(row)

            if self.sum_cores and len(rows_to_print) > 1:
                sum_row = None
                for row in rows_to_print:
                    sum_row = add_lists_with_none(sum_row, row)
                rows_to_print = [sum_row]


        for rownum, row in enumerate(rows_to_print):

            # header
            if rownum == 0:
                T.print(device.get_name().ljust(sidebar_chars) , newline = 0)
            else:
                T.print("".ljust(sidebar_chars) , newline = 0)

            T.print(" ", newline = 0)

            # sum
            if rownum == 0:
                total_sum = sum_list([ sum_list(row) for row in rows_to_print ])
                T.print(print_count(total_sum).ljust(cpu_count_chars), curses.color_pair(color), newline = 0)
            else:
                 T.print("".ljust(cpu_count_chars), curses.color_pair(color), newline = 0)

            T.print(" ", newline = 0)

            # values
            if self.max_columns < len(row):
                rounded_up_len = (int((len(row) - 1) / self.max_columns) + 1) * self.max_columns
                extend_len = rounded_up_len - len(row)
                if extend_len > 0:
                    row += [ None] * extend_len

            for i, val in enumerate(row):

                # add highlight
                finalcolor = color

                if val != None and val > 0 and not self.totalmode:
                    finalcolor = color + 1

                if val == None:
                    s = "x"
                else:
                    s = print_count(val)


                self.__print_conditional_newline(i, curses.color_pair(color))
                self.__print_conditional_space(i)

                extra = cpu_count_chars - len(s) + 1
                T.print(s, curses.color_pair(finalcolor), newline = 0)
                T.print(" " * extra , curses.color_pair(color), newline = 0)


            # footer
            if rownum == 0:
                T.print(" " + str(device.get_irq_count()).ljust(3), newline = 0)
                T.print(" " + ",".join(device.get_drivers()), newline = 0)
                #T.print(" (" + ",".join(device.get_irq_names()) + ")" , newline = 0)

            T.print("")

    ################################################################################################

    def print_all(self, header):

        if header:
            self.__print_app_header()

        if len(self.devices) == 0:
            return

        self.__print_header()

        for i, devicename in enumerate(sorted(self.devices.keys())):
            device = self.devices[devicename]
            color = 1 if i % 2 == 0 else 10
            self.__print_device(device, color)
        
        T.print("")


