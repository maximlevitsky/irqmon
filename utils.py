#!/usr/bin/python3

# SPDX-License-Identifier: GPL-2.0-or-later
# top-like utility for displaying kvm statistics

def read_file(path):
    with open(path, "r") as file:
        return file.read().strip()


def parse_ranges_file(path):
    data = read_file(path)
    return parse_ranges_str(data)

def parse_ranges_str(data):
    result = []

    for r in data.split(","):
        if '-' in r:
            a,b = r.split('-')
            result += list(range(int(a), int(b) + 1))
        else:
            result.append(int(r))

    return result

def int_array(a):
    return [ int(item) for item in a ]


#####################################################################
def print_count(value):
    if value < 10000:
        return "{:d}".format(value)
    elif value < 10000000:
        return "{:d}k".format(int(value / 1000))
    else:
        return "{:d}m".format(int(value / 1000000))


#####################################################################
# list operations
#####################################################################
def sub_lists(list1, list2):

    if list1 == None and list2 == None:
        return None
    if list2 == None:
        return list(list1)
    if list1 == None:
        return [ -v for v in list2]

    assert(len(list1) == len(list2))
    return [v1 - v2 for (v1, v2) in zip(list1, list2)]

def add_lists(list1, list2):
    if list1 == None and list2 == None:
        return None
    if list1 == None:
        return list(list2)
    if list2 == None:
        return list(list1)

    assert(len(list1) == len(list2))
    return [v1 + v2 for (v1, v2) in zip(list1, list2)]


def add_lists_with_none(list1, list2):
    if list1 == None and list2 == None:
        return None
    if list1 == None:
        return list(list2)
    if list2 == None:
        return list(list1)

    assert(len(list1) == len(list2))
    return [ (0 if v1 == None else v1) + (0 if v2 == None else v2) for (v1, v2) in zip(list1, list2)]


def int_div_list_with_value(list1, value):
    if list1 == None or value == None:
        return None

    return [round(v1 / value) for v1 in list1]


def sum_list(list1):
    if list1 == None:
        return 0
    sum = 0
    for e in list1:
        if e != None:
            sum += e
    return sum


def get_num_online_cpus():
    return len(parse_ranges_file("/sys/devices/system/cpu/online"))
