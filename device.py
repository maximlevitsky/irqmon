#!/usr/bin/python3

# SPDX-License-Identifier: GPL-2.0-or-later
# top-like utility for displaying kvm statistics
from utils import *

##################################################################################
# This class represents a device with one or more IRQs
##################################################################################

class Device:
    #####################################
    def __init__(self, name):
        self.name = name
        self.irqs = []
        self.drivers = None
        
    def add_irq(self, irq):
        assert not irq in self.irqs
        self.irqs.append(irq)

    #####################################

    def get_name(self):
        return self.name

    # this reads the total sum of all interrupts of this device
    def read(self, total = False):
        result = None
        for I in self.irqs:
            value = I.read(total)
            result = add_lists(result, value)

        return result

    # this returns set of drivers for the device (usually one)
    def get_drivers(self):
        if self.drivers == None:
            self.drivers = set()
            for I in self.irqs:
                self.drivers |= I.get_drivers()

        return self.drivers

    # read number of IRQs that this device has
    def get_irq_count(self):
        return len(self.irqs)

    def get_irq_names(self):
        return [ I.get_name() for I in self.irqs]
