#!/usr/bin/python3

# SPDX-License-Identifier: GPL-2.0-or-later
# top-like utility for displaying kvm statistics

import curses
import atexit
import sys

class Terminal:

    def init(self):
        self.stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        atexit.register(self._cleanup)
        self.stdscr.clear()
        sys.excepthook = self.exception_handler

        self.init_color()

    def init_color(self):
        curses.start_color()
        curses.use_default_colors()

        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_CYAN)
        curses.init_pair(10, curses.COLOR_WHITE, curses.COLOR_BLACK)

        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_GREEN)
        curses.init_pair(11, curses.COLOR_BLACK, curses.COLOR_GREEN)


    def exception_handler(self, exctype, value, tb):
        self._cleanup()
        if exctype is KeyboardInterrupt:
            exit(0)
        sys.__excepthook__(exctype, value, tb)

    def _cleanup(self):
        curses.nocbreak()
        curses.echo()
        curses.endwin()

    ################################################################################################

    def clear(self, full = False):
        self.line = 0
        self.column = 0
        if full:
            self.stdscr.erase()

    def print(self, string, option = 0, newline = 1):

        height, width = self.stdscr.getmaxyx()

        if newline:
            extra_len = width - self.column - len(string)
            string += " " * extra_len

            try:
                self.stdscr.addstr(self.line, self.column, string, option)
            except:
                pass
            self.line += 1
            self.column = 0

        else:
            try:
                self.stdscr.addstr(self.line, self.column, string, option)
            except:
                pass
            self.column += len(string)

    def flush(self):
        self.stdscr.refresh()


    def set_timeout(self, miliseconds):
        self.stdscr.timeout(miliseconds)

    def getinputkey(self):
        try:
            return self.stdscr.get_wch()
        except curses.error:
            return None


    ################################################################################################

T = Terminal()
