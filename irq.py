#!/usr/bin/python3

# SPDX-License-Identifier: GPL-2.0-or-later
# top-like utility for displaying kvm statistics

from utils import *

# Class that represents a single IRQ
class IRQ:
    def __init__(self, irqchip_name, name):
        self.irqchip_name = irqchip_name
        self.name = name
        self.hidden = False

        self.initial_value = None
        self.last_value = None
        self.current_value = None

        self.old_timestamp = None
        self.new_timestamp = None

        self.drivers = set()
        self.devices = set()

    def get_name(self):
        return self.name

    def get_irqchip_name(self):
        return self.irqchip_name


    def add_driver(self, driver):
        self.drivers.add(driver)
    def add_device(self, device):
        self.devices.add(device)

    def get_devices(self):
        return self.devices
    def get_drivers(self):
        return self.drivers

    def set_hidden(self):
        self.hidden = True
    def get_hidden(self):
        return self.hidden

    def reset(self):
        self.initial_value = self.current_value
        self.last_value = self.current_value

    ################################################################################################

    def update(self, new_value, new_timestamp):

        if self.initial_value == None:
            self.initial_value = new_value
            self.last_value = new_value
            self.current_value = new_value
            self.old_timestamp = new_timestamp
            self.new_timestamp = new_timestamp
            return

        self.old_timestamp = self.new_timestamp
        self.new_timestamp = new_timestamp

        self.last_value = self.current_value
        self.current_value = new_value


    def read(self, total = False):

        if self.old_timestamp == None or self.new_timestamp == None:
            return None

        if total:
            result = sub_lists(self.current_value, self.initial_value)
        else:
            result = sub_lists(self.current_value, self.last_value)
            result = int_div_list_with_value(result, (self.new_timestamp - self.old_timestamp))

        return result
