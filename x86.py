


def get_x86_irq_info(cpuinfo, use_apic_extra):

    apicv = cpuinfo.has_apicv
    hypervisor = cpuinfo.has_hypervisor

    return {
        # Hardware APIC interrupts
        "LOC" : {"show":1, "irqchip": "LAPIC", "name": "TIMER"},
        "NMI" : {"show":1, "irqchip": "LAPIC", "name": "NMI"},

        # Hardware error interrupts, not intersted
        "TRM" : {"show":use_apic_extra, "irqchip": "LAPIC", "name": "Thermal"},
        "MCE" : {"show":use_apic_extra, "irqchip": "LAPIC", "name": "#MC (uncorr)"},
        "THR" : {"show":use_apic_extra, "irqchip": "LAPIC", "name": "#MC (corr)"},
        "DFR" : {"show":use_apic_extra, "irqchip": "LAPIC", "name": "#MC (deffered)"},

        # IRQ_WORK_VECTOR / sysvec_irq_work
        # only sent locally as self-ipi
        "IWI" : {"show":1, "irqchip": "LAPIC", "name": "IRQ work"},

        # CALL_FUNCTION_VECTOR / sysvec_call_function
        # CALL_FUNCTION_SINGLE_VECTOR / sysvec_call_function_single
        # used by KVM to kick group of vCPUs
        "CAL" : {"show":1, "irqchip": "LAPIC", "name": "IPI (function call)"},

        # RESCHEDULE_VECTOR / sysvec_reschedule_ipi
        # used by KVM to kick a single vCPU
        "RES" : {"show":1, "irqchip": "LAPIC", "name": "IPI (reschedule)"},

        # flush_tlb_func, called with SMP function call
        "TLB" : {"show":0, "irqchip": "LAPIC", "name": "TLB shootdown"},

        # APICv vectors
        "PIN" : {"show":apicv, "irqchip": "VAPIC", "name": "Target vCPU is in host"},
        "PIW" : {"show":apicv, "irqchip": "VAPIC", "name": "Target vCPU is halted"},
        "NPI" : {"show":apicv, "irqchip": "VAPIC", "name": "Target vCPU is in nested"},
        "PMN": {"show":apicv, "irqchip": "VAPIC", "name": "Non-VM posted interrupts"},

        # hypervisor synthetic vectors
        "HYP" : {"show":hypervisor, "irqchip": "HYPERVISOR", "name": "HV callbacks"},
        "HRE" : {"show":hypervisor, "irqchip": "HYPERVISOR", "name": "HV reenlighments"},

        # Known and hidden interrupt sources
        "PMI" : {"show":0, "irqchip": "LAPIC", "name": "perf overflow"}, # Not an interrupt - NMI triggers it
        "SPU" : {"show":use_apic_extra, "irqchip": "LAPIC", "name": "Spurious int"},
        "RTR" : {"show":0, "irqchip": "LAPIC", "name": "APIC ICR retry"}, # Not an interrupt
        "MCP" : {"show":0, "irqchip": "LAPIC", "name": "#MC (poll)"}, # Not an interrupt

        # junk
        "ERR" : {"show":0, "irqchip": "LAPIC", "name": "APIC Error"},
        "MIS" : {"show":0, "irqchip": "LAPIC", "name": "IOAPIC miss"},

    }
