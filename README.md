# IRQMON - simple x86 interrupt monitoring tool

This is a small utility to help with live monitoring of IRQ activity on x86 systems (both Intel and AMD).

It shows the info from /proc/interrupts in a more compact and readable way.

This is an example screenshot:

![](images/screenshot.png)

## Dependencies

The program was tested with Python 3.8.9.
It optionally uses the cpuinfo library to hide some rows which are not relevant on the target machine.

On fedora/RHEL:

    sudo dnf install python3-cpuinfo

## Command line options:

**--total**
Display total IRQ counts, since the moment the app was started or reset (with **R** key)

**--interval**
The update interval in milliseconds. Defaults is 1000 (1 second).

**--smp {none,group,sum}**
This option allows to reduce size of output on large systems,
by combining together the interrupts of both threads of a cpu core by either printing them in two rows, one above the other,
or printing a single row which contains sum of the values from both threads.

*--smp 'group'* is enabled by default on systems with more that 32 logical CPUs.

**--extra {bridges,apic,softirq}**
This option allows to show some extra interrupt sources which are not shown by default.

This includes PCIe bridges, extra interrupts from local APIC, like machine check, and finally so called softirqs
which in a strict sense are not IRQs but whose monitoring can still be useful.

**--cpus list_of_cpus**
For really large systems, this option can be used to monitor only some cpus. 
You can give the list with ranges, like *0-16,30-40*

**--max_columns N**
Another option which is useful for really large systems. 

Allows each row of output to be split to multiple rows, each having maximum of N columns.

Useful to combine with --smp sum to reduce the number of rows.

Example: *irqmon --smp sum --max_columns 16* (32 cores / 64 threads machine)

![](images/screenshot_max_columns.png)

**--separators**
An option to help visualize system partitions. Allows to insert visual separators between some columns.

Example *irqmon --separators 14 16 28*:

![](images/screenshot_separators.png)

## User interface

While running you can press **SPACE** to pause the output, and you can also press **R** to reset the accumulated output,
when using the **--total** mode.

Each column of output represents the number of IRQs received on the CPU either since the last update
or since the app start (when using the **--total** mode).

Values above 10000 use SI suffixes (K, M).

The last two columns represent the number of IRQs which this device have, and the driver name which is assigned to the device,
both of these are useful for troubleshooting.

## Theory of operation

* On startup, the irqmon reads a list of all hardware interrupts from **/sys/kernel/irq**
  Interrupts are grouped by irqchips, according to info from **/sys/kernel/irq**.

* This interrupt list is amended with a list of x86 specific interrupts from **/proc/interrupts** 
  (interrupts such as NMI, local apic timer, inter process interrupts, etc).

  The tool has a hardcoded x86 table for these interrupts which is used to name them with human readable names. 
 
  The table is also used to hide some irrelevant interrupts (some can still be shown with **--extra apic**) 
 
  All unknown lines from **/proc/interrupts** are still printed AS IS under *MISC* irqchip.

* /sys/bus/pci/devices is scanned for PCI interrupts, to find a pairing between IRQs and devices.

* Finally /proc/interrupts is scanned again for all other numeric interrupts which have no pci device attached.
  These interrupts are attached to a synthetic device with a synthetic name based on the driver name as 
  appears in /proc/interrupts on the last column and the IRQ number.

* After each update, the /proc/interrupts is being read again, and data is presented on the screen. 

  Each PCI device is written as a single row which contains the sum of all interrupts which are attached to it (usually affinity to different CPUs).
  For all other interrupts, they are presented 1:1 as in /proc/interrupts.

If either number of CPUs or number of IRQs in the system changes, the program is reset.

## Further work

* Support non x86 arches
* Visualize IRQ affinity
* Support non PCI devices

