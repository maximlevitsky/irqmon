#!/usr/bin/python3

# SPDX-License-Identifier: GPL-2.0-or-later
# top-like utility for displaying kvm statistics
from utils import *
import os

try:
    import cpuinfo
    cpu_info = cpuinfo.get_cpu_info()
except ImportError:
    cpu_info = None

class CPUInfo(object):
    def __init__(self, ignore_smp, cpu_list = None):

        if cpu_info:
            self.has_apicv = 'posted_intr' in cpu_info['flags']
            self.has_hypervisor = 'hypervisor' in cpu_info['flags']
        else:
            self.has_apicv = True
            self.has_hypervisor = True


        # temp workaround for lack of support for vmx flags in cpuinfo
        if self.has_apicv == False:
            if os.system("cat /proc/cpuinfo  | grep 'vmx flags' | grep 'apicv'") == 0:
                self.has_apicv = True

        # for each (pkg,core) tuple, a list of its threads (logical CPU ids)
        self.core_map = {}

        # maximum number of threads in a core
        self.max_thread_count = 0

        cpudir = "/sys/bus/cpu/devices/"

        self.online_cpus = parse_ranges_file("/sys/devices/system/cpu/online")
        self.proc_irq_to_logical_id_map = {}

        # seq list of all logical cores, which matches the order in /proc/interrupts
        for index, logical_core_id in enumerate(self.online_cpus):
            self.proc_irq_to_logical_id_map[logical_core_id] = index

        # all logical cores ids present in the system
        logical_cpu_ids = sorted([ int(val[3:]) for val in os.listdir(cpudir) ])
        if cpu_list != None:
            logical_cpu_ids = filter(lambda logical_cpu_id: logical_cpu_id in cpu_list, logical_cpu_ids)

        if ignore_smp:
            # thread each CPU as a separate core
            for logical_cpu_id in logical_cpu_ids:
                self.core_map[logical_cpu_id] = [logical_cpu_id]
            self.max_thread_count = 1
            return

        # collect all cores
        for logical_cpu_id in logical_cpu_ids:
            try:
                online = int(read_file(cpudir + "cpu" + str(logical_cpu_id) + "/online"))
                if online != 1:
                    continue
            except:
                pass

            topodir = cpudir + "cpu" + str(logical_cpu_id) + "/topology/"
            core_id = int(read_file(topodir + "/core_id"))
            package_id = int(read_file(topodir + "/physical_package_id"))

            uniq_core_id = (package_id, core_id)

            if not(uniq_core_id in self.core_map):
                self.core_map[uniq_core_id] = list()

            self.core_map[uniq_core_id].append(logical_cpu_id)


        # calculate max thread id (usualy 0 or 1, for dual SMT systems):
        for thread_list in self.core_map.values():
            self.max_thread_count = max(len(thread_list), self.max_thread_count)


    def get_logical_cores_for_thread(self, thread_num):
        # return row all logical core ids (CPU numbers) which have specific thread number (0 or 1)
        result = []

        if thread_num > self.max_thread_count:
            return None

        for threads in self.core_map.values():
            if len(threads) > thread_num:
                result.append(threads[thread_num])
            else:
                result.append(-1)

        return result

    def get_proc_irq_value(self, values, logical_core_id):
        # return value from /proc/irq line which corresponds to given logical core id
        if logical_core_id == -1:
            return None

        if not logical_core_id in self.proc_irq_to_logical_id_map:
            return None

        return values[self.proc_irq_to_logical_id_map[logical_core_id]]
